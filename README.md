# Learnings from LU 8
I learned how and why to use Version Control. As tool, I used GitLab nad uploaded some code files
there as well as created a readme. Version Control essentially works like Google Docs, 
where you can see who made what change when. Also, I learned the basics
about the markup language typically used for Version Control.

## Findings of first trading strategy from LU 7
I implemented a code to run a simple trading strategy. Essentially, this trading strategy worked 
out positively as portfolio value increased from 10000 to 11412 in one month and Sharpe ratio was 
higher than 1 most of the time.


### Findings from Buy and Hold Strategy 
This strategy worked out even better with a final portfolio value of 11184 (up from 10009)
in the same time period. My learning here was that to implement and run the algorithm you also
need to run the funtion "run_algorithm". Otherwise there is no output.

# Links 

You can link to [gitlab] (http://gitlab.com) like this

# Code Formatting

```py

def say_hello(who):
    print('hello', who)
    
````

# Lists

1. ordered list
    
    this list is ordered
3. you see that the second item comes after the first


* unordered list
- here the order really
+ doesn't matter


now Quantopian